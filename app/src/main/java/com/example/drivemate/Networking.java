package com.example.drivemate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Networking {
    static int serverPort = 8246;
    //static String serverIP = "18.216.96.62";  // EC2 - AWS
    //static String serverIP = "10.0.0.22";     // Local IP
    static String serverIP = "51.105.27.210";   // Azure Linux VM
    static Socket socket = null;
    static InetAddress address = null;
    static BufferedReader input = null;
    static OutputStream output = null;
    static boolean error = false;
    static String toSend = null;
    static String answer = null;
    static boolean serverBusy = false;
}

package com.example.drivemate;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class TransitionIntentService extends IntentService {
    protected static boolean gotMatch = false;
    protected static boolean inVehicle = false;
    protected static boolean inSettings = false;
    protected static String[] activities = {"In vehicle", "On bicycle", "On foot", "Still", "Unknown", "Tilting", "", "Walking", "Running"};

    private static final String TAG = TransitionIntentService.class.getSimpleName();

    public TransitionIntentService() {
        super("TransitionIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Connection connection = new Connection();
        connection.start();

        GetMatch matchGetter = new GetMatch();
        matchGetter.start();

        while (connection.isAlive()){}

        if (null != intent) {
            if (ActivityTransitionResult.hasResult(intent)) {
                ActivityTransitionResult result = ActivityTransitionResult.extractResult(intent);
                for (ActivityTransitionEvent event : result.getTransitionEvents()) {
                    if (event.getActivityType() < 9) {
                        showToast(activities[event.getActivityType()]);
                    }

                    if (!inSettings)
                    {
                        if(event.getActivityType() == 0) //something has happened with a vehicle
                        {
                            if(event.getTransitionType() == 0) //the user has entered a vehicle
                            {
                                showToast("Detected driving.");

                                inVehicle = true;

                                while (connection.isAlive()){}

                                Networking.toSend = "DRV###" + Preferences.getUsername(getApplicationContext()) + "###BEGINNING\n";

                                SendMessage messageSender = new SendMessage();
                                messageSender.start();

                                while (messageSender.isAlive()){}

                                //GetMatch matchGetter = new GetMatch();
                                //matchGetter.start();
                            }
                            else if (event.getTransitionType() == 1) //the user has left the vehicle
                            {
                                showToast("You left the vehicle.");

                                inVehicle = false;

                                gotMatch = true;

                                Networking.toSend = "DRV###" + Preferences.getUsername(getApplicationContext()) + "###END\n";
                                SendMessage messageSender = new SendMessage();
                                messageSender.start();
                            }
                        }
                    }
                }
            }
        }
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }

    private class GetMatch extends Thread{
        GetMatch(){}

        public void run(){
            while (true) {
                while (!gotMatch && inVehicle && !inSettings) {
                    showToast("Searching for a match.");

                    Networking.toSend = "MTC###" + Preferences.getUsername(getApplicationContext()) + '\n';

                    SendMessage messageSender = new SendMessage();
                    messageSender.start();

                    while (messageSender.isAlive()) {
                    }

                    GetMessage messageGetter = new GetMessage();
                    messageGetter.start();

                    while (messageGetter.isAlive()) {
                    }

                    processAnswer();

                    if (!gotMatch) {
                        try {
                            Thread.sleep(5 * 1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    Thread.sleep(5 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void processAnswer(){
        if (Networking.answer.contains("###")){
            final String[] parts = Networking.answer.split("###");

            if (parts[0].equals("MATCH")){
                Preferences.setGotMatch(true, getApplicationContext());

                showToast("Yay! you got 10 points!");

                String username = parts[1];
                String phoneNumber = parts[2];
                String age = parts[3];
                String commonInterests = parts[4];
                String commonLanguages = parts[5];
                boolean isActive = parts[6].equals("True");

                Preferences.setMatchName(username, getApplicationContext());
                Preferences.setMatchAge(age, getApplicationContext());
                Preferences.setMatchPhoneNumber(phoneNumber, getApplicationContext());
                Preferences.setMatchCommonInterests(commonInterests, getApplicationContext());
                Preferences.setMatchCommonLanguages(commonLanguages, getApplicationContext());
                Preferences.setIsActive(isActive, getApplicationContext());

                gotMatch = true;

                Intent i = new Intent(getApplicationContext(), profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

                //startActivity(new Intent(TransitionIntentService.this, profile.class));
            }
        }
        else if (Networking.answer.equals("NO_MATCH")){
            gotMatch = false;
        }
    }

    public void showToast(String message) {
        final String msg = message;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }
}

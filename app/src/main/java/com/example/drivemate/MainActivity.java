package com.example.drivemate;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        String[] permissions = {Manifest.permission.READ_CONTACTS,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions(this, permissions,0);

        configureGPSButton();
        // configureSignupButton();
        // configureLoginButton();
    }


    /**
     * Configure the button that opens the first page of the signing up activity.
     */
    private void configureSignupButton() {
        Button btn = findViewById(R.id.signup);
        btn.setBackgroundResource(R.mipmap.btn_gradient);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                startActivity(new Intent(MainActivity.this, SignupPage1.class));

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                if (checkPermissions()){
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                else
                    finishAndRemoveTask();
            }
        });
    }

    private void configureGPSButton() {
        final Button btn = findViewById(R.id.location);

        if (isLocationEnabled(getApplicationContext())) {
            btn.setVisibility(View.INVISIBLE);
            configureLoginButton();
            configureSignupButton();
        }

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                if (!isLocationEnabled(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 1);
                }
            }
        });
    }

    /**
     * Configure the button that opens the login page.
     */
    private void configureLoginButton() {
        TextView btn = findViewById(R.id.login);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                startActivity(new Intent(MainActivity.this, Login.class));

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                if (checkPermissions()){
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                else
                    finishAndRemoveTask();
            }
        });
    }

    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        else if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        else if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        else {
            return isLocationEnabled(getApplicationContext());
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Button btn = findViewById(R.id.location);
        if (isLocationEnabled(getApplicationContext())){
            btn.setVisibility(View.INVISIBLE);
            configureSignupButton();
            configureLoginButton();
        }
    }
}

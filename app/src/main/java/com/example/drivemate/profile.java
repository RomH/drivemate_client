package com.example.drivemate;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;

public class profile extends AppCompatActivity {

    TextToSpeech mTts;
    boolean blocked = false;
    boolean backed = false;
    static final String TAG = "TTS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        mTts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTts.setLanguage(Locale.ENGLISH);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getApplicationContext(), "This language is not supported", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.v("TTS","onInit succeeded");
                        speak("You got a match!");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Initialization failed", Toast.LENGTH_SHORT).show();
                }

            }
        });


        configureBackButton();
        configureBlockButton();
        configureTextViews();

        backed = false;

        CallMatch caller = new CallMatch();
        caller.start();
    }

    void speak(String s){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.v(TAG, "Speak new API");
            Bundle bundle = new Bundle();
            mTts.speak(s, TextToSpeech.QUEUE_FLUSH, bundle, null);
        } else {
            Log.v(TAG, "Speak old API");
            HashMap<String, String> param = new HashMap<>();
            mTts.speak(s, TextToSpeech.QUEUE_FLUSH, param);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        if (vibrator != null) {
            vibrator.vibrate(50);
        }

        //TransitionIntentService.gotMatch = false;
        backed = true;
        TransitionIntentService.gotMatch = false;
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void configureBackButton()
    {
        Button backBtn = findViewById(R.id.backProfile);

        backBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onBackPressed();
            }
        });
    }

    private void configureBlockButton()
    {
        final Button button = findViewById(R.id.block);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Connection connection = new Connection();
                connection.start();

                while (connection.isAlive()){}

                Networking.toSend = buildMessage() + '\n';

                SendMessage messageSender = new SendMessage();
                messageSender.start();

                while (messageSender.isAlive()){}

                GetMessage messageGetter = new GetMessage();
                messageGetter.start();

                while (messageGetter.isAlive()){}

                processAnswer();
            }
        });
    }

    private void configureTextViews(){
        TextView name = findViewById(R.id.nameTXT);
        TextView age = findViewById(R.id.ageTXT);
        TextView phone = findViewById(R.id.phoneTXT);
        TextView langs = findViewById(R.id.langsTXT);
        TextView ints = findViewById(R.id.intTXT);
        int index = 0;
        String[] array;
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<b>Their username is </b>");
        stringBuilder.append(Preferences.getMatchName(getApplicationContext()));
        name.setText(Html.fromHtml(stringBuilder.toString()));
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("<b>Their age is </b>");
        stringBuilder.append(Preferences.getMatchAge(getApplicationContext()));
        age.setText(Html.fromHtml(stringBuilder.toString()));
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("<b>Their phone number is </b>");
        stringBuilder.append(Preferences.getMatchPhoneNumber(getApplicationContext()));
        phone.setText(Html.fromHtml(stringBuilder.toString()));
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("<b>You both speak </b>");
        array = Preferences.getMatchCommonLanguages(getApplicationContext());
        stringBuilder.append(array[0]);
        index++;

        while (index < array.length){
            if (index == array.length - 1){
                stringBuilder.append(" and ");
            }
            else {
                stringBuilder.append(", ");
            }

            stringBuilder.append(array[index++]);
        }

        langs.setText(Html.fromHtml(stringBuilder.toString()));
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("<b>Both of you are interested in </b>");
        array = Preferences.getMatchCommonInterests(getApplicationContext());
        index = 0;
        stringBuilder.append(array[0]);
        index++;

        while (index < array.length){
            if (index == array.length - 1){
                stringBuilder.append(" and ");
            }
            else {
                stringBuilder.append(", ");
            }

            stringBuilder.append(array[index++]);
        }

        ints.setText(Html.fromHtml(stringBuilder.toString()));
    }


    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }

    private String buildMessage(){
        return "BLC" +
                "###" +
                Preferences.getUsername(getApplicationContext()) +
                "###" +
                Preferences.getMatchName(getApplicationContext());
    }

    private void processAnswer(){
        if (null != Networking.answer) {
            final String[] parts = Networking.answer.split("###");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Networking.answer.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Failed to add languages.", Toast.LENGTH_SHORT).show();
                    } else {
                        String result = parts[0];

                        if (result.equals("FAILED")) {
                            Toast.makeText(getApplicationContext(), "Failed to block this user, sorry.", Toast.LENGTH_SHORT).show();
                        } else if (result.equals("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), "Blocked successfully.", Toast.LENGTH_SHORT).show();
                            blocked = true;
                            TransitionIntentService.gotMatch = false;
                            //Toast.makeText(getApplicationContext(), "Searching another match.", Toast.LENGTH_SHORT).show();
                            Preferences.setGotMatch(false, getApplicationContext());
                            startActivity(new Intent(profile.this, running.class));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        } else if (result.equals("USER_ALREADY_BLOCKED")) {
                            // If this happens, there is an error somewhere in the server.
                            Toast.makeText(getApplicationContext(), "This user is already blocked...", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(), "Searching another match.", Toast.LENGTH_SHORT).show();
                            Preferences.setGotMatch(false, getApplicationContext());
                            onBackPressed();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            });
        }

        Networking.answer = null;
    }

    private class CallMatch extends Thread{
        CallMatch(){}

        public void run(){
            try {
                if (Preferences.getIsActive(getApplicationContext())) {
                    Thread.sleep(5 * 1000);

                    if (!blocked && !backed){
                        Intent intent = new Intent(Intent.ACTION_CALL);

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                            //speak("Read contacts permission not granted!");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Read contacts permission not granted.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        } else {
                            if (Preferences.getMatchPhoneNumber(getApplicationContext()) == "phone_number") {
                                //speak("Contact not found. Try again!");
                            } else {
                                intent.setData(Uri.parse("tel:" + Preferences.getMatchPhoneNumber(getApplicationContext())));

                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    //speak("Permission not granted!");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Call phone permission not granted.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    return;
                                } else {
                                    startActivity(intent);
                                }
                            }
                        }

                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

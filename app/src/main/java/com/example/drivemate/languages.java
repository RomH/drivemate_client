package com.example.drivemate;

import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class languages extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languages);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        /*Preferences.setLanguage1("None", getApplicationContext());
        Preferences.setLanguage2("None", getApplicationContext());
        Preferences.setLanguage3("None", getApplicationContext());*/

        Spinner spinner1 = findViewById(R.id.spinner);
        Spinner spinner2 = findViewById(R.id.spinner2);
        Spinner spinner3 = findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.languages, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);

        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        spinner3.setAdapter(adapter);
        spinner3.setOnItemSelectedListener(this);

        configureNextButton();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();

        switch(parent.getId()){
            case R.id.spinner:
                Preferences.setLanguage1(text, getApplicationContext());
                break;
            case R.id.spinner2:
                Preferences.setLanguage2(text, getApplicationContext());
                break;
            case R.id.spinner3:
                Preferences.setLanguage3(text, getApplicationContext());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Configure the button that opens the 'interests' page.
     */
    private void configureNextButton() {
        Button btn = findViewById(R.id.nextLanguages);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                final String lang1 = Preferences.getLanguage1(getApplicationContext());
                final String lang2 = Preferences.getLanguage2(getApplicationContext());
                final String lang3 = Preferences.getLanguage3(getApplicationContext());

                wrongDetails(lang1, lang2, lang3);
            }
        });
    }


    private void wrongDetails(String lang1, String lang2, String lang3)
    {
        if(lang1.equals("None")) //if they haven't filled the first language
        {
            Toast.makeText(getBaseContext(), "Please fill your first language", Toast.LENGTH_LONG).show();

            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            if (vibrator != null) {
                vibrator.vibrate(150);
            }

        }
        else if((lang1.equals(lang2) && !lang1.equals("None") && !lang2.equals("None")) || (lang1.equals(lang3) && !lang1.equals("None")
                && !lang3.equals("None")) || (lang2.equals(lang3) && !lang2.equals("None") && !lang3.equals("None"))) //if there's 2 languages that are the same
        {
            Toast.makeText(getBaseContext(), "You can't choose 2 languages or more that are the same!", Toast.LENGTH_LONG).show();

            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            if (vibrator != null) {
                vibrator.vibrate(150);
            }
        }
        else
        {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

            if (vibrator != null) {
                vibrator.vibrate(50);
            }

            Connection connection = new Connection();
            connection.start();

            while (connection.isAlive()){}

            if (Networking.error){
                Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
            }
            else{
                SendMessage messageSender = new SendMessage();
                Networking.toSend = buildMessage() + '\n';
                messageSender.start();

                while (messageSender.isAlive()){}
//
                if (Networking.error){
                    Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
                }
                else{
                    GetMessage messageGetter = new GetMessage();
                    messageGetter.start();
//
                    while (messageGetter.isAlive()){}

                    if (Networking.error){
                        Toast.makeText(getApplicationContext(), "Can't get a message from the server, sorry.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        processAnswer();
                    }
                }
            }
        }
    }

    private String buildMessage(){
        return "LNG" +
                "###" +
                Preferences.getUsername(getApplicationContext()) +
                "###" +
                Preferences.getLanguage1(getApplicationContext()) +
                "###" +
                Preferences.getLanguage2(getApplicationContext()) +
                "###" +
                Preferences.getLanguage3(getApplicationContext());
    }

    private void processAnswer(){
        if (null != Networking.answer) {
            final String[] parts = Networking.answer.split("###");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Networking.answer.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Failed to add languages.", Toast.LENGTH_SHORT).show();
                    } else {
                        String result = parts[0];
                        String successes = parts[1];
                        String numOfLanguages = parts[2];

                        if (result.equals("FAILED")) {
                            Toast.makeText(getApplicationContext(), "Failed to add languages.", Toast.LENGTH_SHORT).show();
                        } else if (result.equals("SUCCESS")) {
                            if (successes.equals(numOfLanguages)) {
                                Toast.makeText(getApplicationContext(), "Got it.", Toast.LENGTH_SHORT).show();
                            } else {
                                String message = "Added " + successes + " languages (out of " + numOfLanguages + ").";
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }

                            startActivity(new Intent(languages.this, SignupPage2.class));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            });
        }

        Networking.answer = null;
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    private static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }
}

package com.example.drivemate;

import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TransitionIntentService.inSettings = true;

        Connection connection = new Connection();
        connection.start();

        while (connection.isAlive()){}

        if (Networking.error){
            Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
        }
        else{
            SendMessage messageSender = new SendMessage();
            Networking.toSend = "BSY###" + Preferences.getUsername(getApplicationContext()) + "###True\n";
            messageSender.start();

            while (messageSender.isAlive()){}

            if (Networking.error){
                Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
            }
        }

        setHelloSetting();
        setRankingPointsSetting();
        configureBackButton();
        configureInterestsButton();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TransitionIntentService.inSettings = false;

        Connection connection = new Connection();
        connection.start();

        while (connection.isAlive()){}

        if (Networking.error){
            Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
        }
        else{
            SendMessage messageSender = new SendMessage();
            Networking.toSend = "BSY###" + Preferences.getUsername(getApplicationContext()) + "###False\n";
            messageSender.start();

            while (messageSender.isAlive()){}

            if (Networking.error){
                Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
            }
        }

        // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        // Intent i=new Intent(this, running.class);
        // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // startActivity(i);
    }

    private void setHelloSetting()
    {
        TextView hello = findViewById(R.id.usernameSettings);
        hello.append(Preferences.getUsername(getApplicationContext()));
    }

    private void setRankingPointsSetting()
    {
        TextView RP = findViewById(R.id.rankingPoints);
        //int rankingPoints = Preferences.getRankingPoints(getApplicationContext());
        //RP.append(String.valueOf(rankingPoints));

        Connection connection = new Connection();
        connection.start();

        while (connection.isAlive()){}

        if (Networking.error){
            Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
        }
        else{
            SendMessage messageSender = new SendMessage();
            Networking.toSend = "PNT###" + Preferences.getUsername(getApplicationContext()) + '\n';
            messageSender.start();

            while (messageSender.isAlive()){}

            if (Networking.error){
                Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
            }
            else{
                GetMessage messageGetter = new GetMessage();
                messageGetter.start();

                while (messageGetter.isAlive()){}

                if (Networking.error){
                    Toast.makeText(getApplicationContext(), "Can't get a message from the server, sorry.", Toast.LENGTH_LONG).show();
                }
                else {
                    if (Networking.answer.contains("###")){
                        final String[] parts = Networking.answer.split("###");

                        if (parts[0].equals("POINTS")){
                            RP.append(parts[1]);
                        }
                    }
                    else if (Networking.answer.equals("USER_DO_NOT_EXIST")){
                        Toast.makeText(getApplicationContext(), "Who are you?", Toast.LENGTH_SHORT);
                    }
                }
            }
        }
    }

    private void configureInterestsButton()
    {
        Button interests = findViewById(R.id.button);

        interests.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                startActivity(new Intent(settings.this, settingInterests.class));

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    private void configureBackButton()
    {
        Button backBtn = findViewById(R.id.backSettings);

        backBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onBackPressed();
            }
        });
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }
}

package com.example.drivemate;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (!isLocationEnabled(getApplicationContext())){
            finishAndRemoveTask();
        }

        if (null != Networking.socket){
            try {
                Networking.socket.close();
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't close the current socket.", Toast.LENGTH_SHORT).show();
            }
            finally {
                Networking.socket = null;
            }
        }

        configureLoginButton();
    }

    @Override
    public void onBackPressed(){
        Intent i=new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    /**
     * Configure the button that opens the 'running' page.
     */
    private void configureLoginButton() {
        TextView btn = findViewById(R.id.loginBtn);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                if (null != Networking.socket){
                    try {
                        Networking.socket.close();
                    }
                    catch (IOException e){

                    }
                    finally {
                        Networking.socket = null;
                    }
                }

                Connection connection = new Connection();
                connection.start();

                while (connection.isAlive()){}

                if (Networking.error){
                    Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
                }
                else{
                    SendMessage messageSender = new SendMessage();
                    Networking.toSend = buildMessage() + '\n';
                    messageSender.start();

                    while (messageSender.isAlive()){}
//
                    if (Networking.error){
                        Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
                    }
                    else{
                        GetMessage messageGetter = new GetMessage();
                        messageGetter.start();
//
                        while (messageGetter.isAlive()){}

                        if (Networking.error){
                            Toast.makeText(getApplicationContext(), "Can't get a message from the server, sorry.", Toast.LENGTH_LONG).show();
                        }
                        else {
                            processAnswer();
                        }
                    }
                }
            }
        });
    }

    private String buildMessage(){
        final EditText username = findViewById(R.id.usernameLogin);
        final EditText password = findViewById(R.id.passwordLogin);


        return "LOG" +
                "###" +
                username.getText().toString() +
                "###" +
                password.getText().toString();
    }

    private void processAnswer(){
        final EditText user = findViewById(R.id.usernameLogin);

        if (null != Networking.answer) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Networking.answer.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Registration failed.", Toast.LENGTH_SHORT).show();
                        if (null != Networking.socket){
                            try {
                                Networking.socket.close();
                            }
                            catch (IOException e){

                            }
                            finally {
                                Networking.socket = null;
                            }
                        }
                    } else if (Networking.answer.equals("WRONG_USERNAME")) {
                        Toast.makeText(getApplicationContext(), "Error: Username does not exist.", Toast.LENGTH_SHORT).show();
                        if (null != Networking.socket){
                            try {
                                Networking.socket.close();
                            }
                            catch (IOException e){

                            }
                            finally {
                                Networking.socket = null;
                            }
                        }
                    } else if (Networking.answer.equals("WRONG_PASSWORD")) {
                        Toast.makeText(getApplicationContext(), "Error: Wrong password", Toast.LENGTH_SHORT).show();
                    } else if (Networking.answer.equals("USER_ONLINE")) {
                        Toast.makeText(getApplicationContext(), "Error: The user is online.", Toast.LENGTH_SHORT).show();
                        if (null != Networking.socket){
                            try {
                                Networking.socket.close();
                            }
                            catch (IOException e){

                            }
                            finally {
                                Networking.socket = null;
                            }
                        }
                    } else if (Networking.answer.equals("SUCCESS")) {
                        Preferences.setUsername(user.getText().toString(), getApplicationContext());
                        Toast.makeText(getApplicationContext(), "Welcome!", Toast.LENGTH_SHORT).show();
                        //startActivity(new Intent(Login.this, RunningV2.class));
                        startActivity(new Intent(Login.this, running.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }
            });
        }

        Networking.answer = null;
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }

    private static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }
}

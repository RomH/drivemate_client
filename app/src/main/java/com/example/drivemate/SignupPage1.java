package com.example.drivemate;

import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class SignupPage1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page1);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (null != Networking.socket){
            try {
                Networking.socket.close();
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't close the current socket.", Toast.LENGTH_SHORT).show();
            }
            finally {
                Networking.socket = null;
            }
        }

        configureNextPageButton();
    }

    /**
     * Configure the button that opens the next page activity.
     */
    private void configureNextPageButton() {

        final EditText user = (EditText)findViewById(R.id.editUsername);
        final EditText pass = (EditText)findViewById(R.id.password);
        final EditText phone = (EditText)findViewById(R.id.phoneNum);
        final EditText age = (EditText)findViewById(R.id.age);

        Button btn = findViewById(R.id.nextSignup);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                if(user.getText().toString().matches("") || pass.getText().toString().matches("") || phone.getText().toString().matches("") //checking that all the fields are filled
                        || age.getText().toString().matches(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill all the fields", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!checkPass(pass.getText().toString())) //password check
                {
                    Toast.makeText(getApplicationContext(),"Password has to include at least 1 capital letter, lower-case letter and a number", Toast.LENGTH_LONG).show();
                    return;
                }

                if(user.getText().toString().contains(" ") || user.getText().toString().length() < 3) //username check
                {
                    Toast.makeText(getApplicationContext(),"Username has to be at least 3 characters long and can't contain a space!", Toast.LENGTH_LONG).show();
                    return;
                }

                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                if (null != Networking.socket){
                    try {
                        Networking.socket.close();
                    }
                    catch (IOException e){

                    }
                    finally {
                        Networking.socket = null;
                    }
                }

                Connection connection = new Connection();
                connection.start();

                while (connection.isAlive()){}

                if (Networking.error){
                    Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
                }
                else{
                    SendMessage messageSender = new SendMessage();
                    Networking.toSend = buildMessage() + '\n';
                    messageSender.start();

                    while (messageSender.isAlive()){}

                    if (Networking.error){
                        Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
                    }
                    else{
                        GetMessage messageGetter = new GetMessage();
                        messageGetter.start();
//
                        while (messageGetter.isAlive()){}

                        if (Networking.error){
                            Toast.makeText(getApplicationContext(), "Can't get a message from the server, sorry.", Toast.LENGTH_LONG).show();
                        }
                        else {
                            processAnswer();
                        }
                    }
                }
            }
        });
    }

    private static boolean checkPass(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for(int i=0;i < str.length();i++) {
            ch = str.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            }
            else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if(numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }

    private String buildMessage(){
        final EditText username = findViewById(R.id.editUsername);
        final EditText password = findViewById(R.id.password);
        final EditText phone_number = findViewById(R.id.phoneNum);
        final EditText age = findViewById(R.id.age);

        return "REG" +
                "###" +
                username.getText().toString() +
                "###" +
                password.getText().toString() +
                "###" +
                phone_number.getText().toString() +
                "###" +
                age.getText().toString();
    }

    private void processAnswer(){
        final EditText user = findViewById(R.id.editUsername);

        if (null != Networking.answer) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Networking.answer.isEmpty() || Networking.answer.equals("FAILED")) {
                        Toast.makeText(getApplicationContext(), "Registration failed.", Toast.LENGTH_SHORT).show();
                    } else if (Networking.answer.equals("USER_EXISTS")) {
                        Toast.makeText(getApplicationContext(), "This username is taken, please choose a new username.", Toast.LENGTH_SHORT).show();
                    } else if (Networking.answer.equals("SUCCESS")) {
                        Preferences.setUsername(user.getText().toString(), getApplicationContext());
                        Toast.makeText(getApplicationContext(), "Registered!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SignupPage1.this, languages.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }
            });
        }

        Networking.answer = null;
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    private static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }
}

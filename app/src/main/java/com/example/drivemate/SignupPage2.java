package com.example.drivemate;

import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class SignupPage2 extends AppCompatActivity {

    CheckBox checkBox1;
    CheckBox checkBox2;
    CheckBox checkBox3;
    CheckBox checkBox4;
    CheckBox checkBox5;
    CheckBox checkBox6;
    CheckBox checkBox7;
    CheckBox checkBox8;
    CheckBox checkBox9;
    CheckBox checkBox10;
    CheckBox checkBox11;
    CheckBox checkBox12;
    CheckBox checkBox13;
    CheckBox checkBox14;
    CheckBox checkBox15;
    CheckBox checkBox16;
    CheckBox checkBox17;
    CheckBox checkBox18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page2);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        configureFinishButton();


        checkBox1 = findViewById(R.id.Relationships);
        checkBox2 = findViewById(R.id.Sports);
        checkBox3 = findViewById(R.id.Politics);
        checkBox4 = findViewById(R.id.Security);
        checkBox5 = findViewById(R.id.vacations);
        checkBox6 = findViewById(R.id.Trips);
        checkBox7 = findViewById(R.id.Food);
        checkBox8 = findViewById(R.id.family);
        checkBox9 = findViewById(R.id.Fashion);
        checkBox10 = findViewById(R.id.Music);
        checkBox11 = findViewById(R.id.Innovation);
        checkBox12 = findViewById(R.id.Economics);
        checkBox13 = findViewById(R.id.Religion);
        checkBox14 = findViewById(R.id.History);
        checkBox15 = findViewById(R.id.Vehicles);
        checkBox16 = findViewById(R.id.Education);
        checkBox17 = findViewById(R.id.Health);
        checkBox18 = findViewById(R.id.Media);

        checkBox1.setChecked(Preferences.getRelationships(getApplicationContext()));
        checkBox2.setChecked(Preferences.getSports(getApplicationContext()));
        checkBox3.setChecked(Preferences.getPolitics(getApplicationContext()));
        checkBox4.setChecked(Preferences.getSecurity(getApplicationContext()));
        checkBox5.setChecked(Preferences.getVacations(getApplicationContext()));
        checkBox6.setChecked(Preferences.getTrips(getApplicationContext()));
        checkBox7.setChecked(Preferences.getFood(getApplicationContext()));
        checkBox8.setChecked(Preferences.getFamily(getApplicationContext()));
        checkBox9.setChecked(Preferences.getFasion(getApplicationContext()));
        checkBox10.setChecked(Preferences.getMusic(getApplicationContext()));
        checkBox11.setChecked(Preferences.getInnovation(getApplicationContext()));
        checkBox12.setChecked(Preferences.getEconomics(getApplicationContext()));
        checkBox13.setChecked(Preferences.getReligion(getApplicationContext()));
        checkBox14.setChecked(Preferences.getHistory(getApplicationContext()));
        checkBox15.setChecked(Preferences.getVehicles(getApplicationContext()));
        checkBox16.setChecked(Preferences.getEducation(getApplicationContext()));
        checkBox17.setChecked(Preferences.getHealth(getApplicationContext()));
        checkBox18.setChecked(Preferences.getMedia(getApplicationContext()));
    }


    /**
     * Configure the button that finishes the signing up process.
     */
    private void configureFinishButton() {
        final int signupRP = 100;

        Button btn = findViewById(R.id.finishSignup);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                Preferences.setRelationships(checkBox1.isChecked(), getApplicationContext());
                Preferences.setSports(checkBox2.isChecked(), getApplicationContext());
                Preferences.setPolitics(checkBox3.isChecked(), getApplicationContext());
                Preferences.setSecurity(checkBox4.isChecked(), getApplicationContext());
                Preferences.setVacations(checkBox5.isChecked(), getApplicationContext());
                Preferences.setTrips(checkBox6.isChecked(), getApplicationContext());
                Preferences.setFood(checkBox7.isChecked(), getApplicationContext());
                Preferences.setFamily(checkBox8.isChecked(), getApplicationContext());
                Preferences.setFashion(checkBox9.isChecked(), getApplicationContext());
                Preferences.setMusic(checkBox10.isChecked(), getApplicationContext());
                Preferences.setInnovation(checkBox11.isChecked(), getApplicationContext());
                Preferences.setEconomics(checkBox12.isChecked(), getApplicationContext());
                Preferences.setReligion(checkBox13.isChecked(), getApplicationContext());
                Preferences.setHistory(checkBox14.isChecked(), getApplicationContext());
                Preferences.setVehicles(checkBox15.isChecked(), getApplicationContext());
                Preferences.setEducation(checkBox16.isChecked(), getApplicationContext());
                Preferences.setHealth(checkBox17.isChecked(), getApplicationContext());
                Preferences.setMedia(checkBox18.isChecked(), getApplicationContext());

                Preferences.setRankningPoints(signupRP, getApplicationContext());
                Toast.makeText(getApplicationContext(),"Congratulations! You just got 100 Ranking Points for signing up!", Toast.LENGTH_LONG).show();

                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                Connection connection = new Connection();
                connection.start();

                while (connection.isAlive()){}

                if (Networking.error){
                    Toast.makeText(getApplicationContext(), "Can't connect to the server, sorry.", Toast.LENGTH_LONG).show();
                }
                else{
                    SendMessage messageSender = new SendMessage();
                    Networking.toSend = buildMessage() + '\n';
                    messageSender.start();

                    while (messageSender.isAlive()){}
//
                    if (Networking.error){
                        Toast.makeText(getApplicationContext(), "Can't send a message to the server, sorry.", Toast.LENGTH_LONG).show();
                    }
                    else{
                        GetMessage messageGetter = new GetMessage();
                        messageGetter.start();
//
                        while (messageGetter.isAlive()){}

                        if (Networking.error){
                            Toast.makeText(getApplicationContext(), "Can't get a message from the server, sorry.", Toast.LENGTH_LONG).show();
                        }
                        else {
                            processAnswer();
                        }
                    }
                }
            }
        });
    }

    private String buildMessage(){
        final String username = Preferences.getUsername(getApplicationContext());

        StringBuilder message = new StringBuilder();

        message.append("INT");
        message.append("###");
        message.append(username);

        if (Preferences.getRelationships(getApplicationContext())){
            message.append("###");
            message.append("Relationships");
        }

        if (Preferences.getSports(getApplicationContext())){
            message.append("###");
            message.append("Sports");
        }

        if (Preferences.getPolitics(getApplicationContext())){
            message.append("###");
            message.append("Politics");
        }

        if (Preferences.getSecurity(getApplicationContext())){
            message.append("###");
            message.append("Security");
        }

        if (Preferences.getVacations(getApplicationContext())){
            message.append("###");
            message.append("Vacations");
        }

        if (Preferences.getTrips(getApplicationContext())){
            message.append("###");
            message.append("Trips");
        }

        if (Preferences.getFood(getApplicationContext())){
            message.append("###");
            message.append("Food");
        }

        if (Preferences.getFamily(getApplicationContext())){
            message.append("###");
            message.append("Family");
        }

        if (Preferences.getFasion(getApplicationContext())){
            message.append("###");
            message.append("Fashion");
        }

        if (Preferences.getMusic(getApplicationContext())){
            message.append("###");
            message.append("Music");
        }

        if (Preferences.getInnovation(getApplicationContext())){
            message.append("###");
            message.append("Innovation");
        }

        if (Preferences.getEconomics(getApplicationContext())){
            message.append("###");
            message.append("Economics");
        }

        if (Preferences.getReligion(getApplicationContext())){
            message.append("###");
            message.append("Religion");
        }

        if (Preferences.getHistory(getApplicationContext())){
            message.append("###");
            message.append("History");
        }

        if (Preferences.getVehicles(getApplicationContext())){
            message.append("###");
            message.append("Vehicles");
        }

        if (Preferences.getEducation(getApplicationContext())){
            message.append("###");
            message.append("Education");
        }

        if (Preferences.getHealth(getApplicationContext())){
            message.append("###");
            message.append("Health");
        }

        if (Preferences.getMedia(getApplicationContext())){
            message.append("###");
            message.append("Media");
        }

        return message.toString();
    }

    private void processAnswer(){
        if (null != Networking.answer) {
            final String[] parts = Networking.answer.split("###");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Networking.answer.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Failed to add interests.", Toast.LENGTH_SHORT).show();
                    } else {
                        String result = parts[0];
                        String successes = parts[1];
                        String interests = parts[2];

                        if (result.equals("FAILED")) {
                            Toast.makeText(getApplicationContext(), "Failed to add interests.", Toast.LENGTH_SHORT).show();
                        } else if (result.equals("SUCCESS")) {
                            if (successes.equals(interests)) {
                                Toast.makeText(getApplicationContext(), "Saved!", Toast.LENGTH_SHORT).show();
                            } else {
                                String message = "Added " + successes + " interests (out of " + interests + ").";
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }

                            startActivity(new Intent(SignupPage2.this, Login.class));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            });
        }

        Networking.answer = null;
    }

    private class Connection extends Thread{
        Connection(){
            try {
                Networking.address = InetAddress.getByName(Networking.serverIP);
            }
            catch (UnknownHostException e){
                Networking.address = null;
                Networking.error = true;
            }
        }

        public void run(){
            if (null != Networking.address) {
                try {
                    while (Networking.serverBusy){}

                    Networking.serverBusy = true;
                    if (null == Networking.socket) {
                        Networking.socket = new Socket(Networking.address, Networking.serverPort);
                    }
                    Networking.serverBusy = false;
                }
                catch (IOException e){
                    Networking.socket = null;
                    Networking.error = true;
                    Networking.serverBusy = false;
                }

                if (null != Networking.socket){
                    try{
                        while (Networking.serverBusy){}

                        Networking.serverBusy = true;
                        Networking.input = new BufferedReader(new InputStreamReader(Networking.socket.getInputStream()));
                        Networking.output = Networking.socket.getOutputStream();
                        Networking.serverBusy = false;

                        Networking.error = false;
                    }
                    catch (IOException e){
                        Networking.input = null;
                        Networking.output = null;
                        Networking.error = true;
                        Networking.serverBusy = false;
                    }
                }
            }
        }
    }

    private class SendMessage extends Thread{
        SendMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                if (null != Networking.toSend){
                    try {
                        Networking.output.write(Networking.toSend.getBytes());
                    } catch (IOException e) {
                        Networking.error = true;
                    }
                }

                Networking.toSend = null;
                Networking.serverBusy = false;
            }
        }
    }

    static class GetMessage extends Thread{
        GetMessage(){}

        public void run(){
            if (!Networking.error){
                while (Networking.serverBusy){}

                Networking.serverBusy = true;

                try {
                    Networking.answer = Networking.input.readLine();
                } catch (IOException e) {
                    Networking.error = true;
                    Networking.answer = null;
                }

                Networking.serverBusy = false;
            }
        }
    }
}

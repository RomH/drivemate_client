package com.example.drivemate;

import android.content.Context;

class Preferences {
    static boolean getIsActive(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("isActive", false);
    }

    static Integer getServerPort(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getInt("server_port", 49151);
    }

    static boolean getRelationships(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("relationships", false);
    }

    static boolean getGotMatch(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("gotMatch", false);
    }

    static boolean getPolitics(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("politics", false);
    }

    static boolean getSecurity(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("security", false);
    }

    static boolean getSports(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("sports", false);
    }

    static boolean getVacations(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("vacations", false);
    }

    static boolean getTrips(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("trips", false);
    }

    static boolean getFood(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("food", false);
    }

    static boolean getFamily(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("family", false);
    }

    static boolean getFasion(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("fashion", false);
    }

    static boolean getMusic(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("music", false);
    }

    static boolean getInnovation(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("innovation", false);
    }

    static boolean getEconomics(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("economics", false);
    }

    static boolean getReligion(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("religion", false);
    }

    static boolean getHistory(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("history", false);
    }

    static boolean getVehicles(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("vehicles", false);
    }

    static boolean getEducation(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("education", false);
    }

    static boolean getHealth(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("health", false);
    }

    static boolean getMedia(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("media", false);
    }

    static int getRankingPoints(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getInt("points", 0);
    }

    static String getUsername(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("username", "");
    }

    static String getLanguage1(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("language1", "None");
    }

    static String getLanguage2(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("language2", "None");
    }

    static String getLanguage3(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("language3", "None");
    }

    static String getMatch(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("match", "None");
    }

    static String getMatchAge(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE)
                .getString("match_age", "age");
    }

    static String getMatchName(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE)
                .getString("match_name", "username");
    }

    static String getMatchPhoneNumber(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE)
                .getString("match_phone_number", "phone_number");
    }

    static String getIsDriving(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getString("driving", "END");
    }

    static String[] getMatchCommonInterests(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE)
                .getString("match_com_int", "com_int").split("@@@");
    }

    static String[] getMatchCommonLanguages(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE)
                .getString("match_langs", "com_lan").split("@@@");
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------

    static void setIsActive(boolean active, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("isActive", active).apply();
    }

    static void setLanguage1(String language1, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("language1", language1).apply();
    }

    static void setLanguage2(String language2, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("language2", language2).apply();
    }

    static void setLanguage3(String language3, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("language3", language3).apply();
    }

    static void setUsername(String username, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("username", username).apply();
    }

    static void setMatch(String match, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match", match).apply();
    }

    static void setMatchAge(String age, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match_age", age).apply();
    }

    static void setMatchName(String name, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match_name", name).apply();
    }

    static void setMatchPhoneNumber(String number, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match_phone_number", number).apply();
    }

    static void setMatchCommonInterests(String interests, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match_com_int", interests).apply();
    }

    static void setMatchCommonLanguages(String languages, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("match_langs", languages).apply();
    }

    static void setDriving(String state, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("driving", state).apply();
    }

    static void setRankningPoints(int points, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putInt("points", points).apply();
    }

    static void setRelationships(boolean relationships, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("relationships", relationships).apply();
    }

    static void setGotMatch(boolean gotMatch, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("gotMatch", gotMatch).apply();
    }

    static void setPolitics(boolean politics, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("politics", politics).apply();
    }

    static void setSecurity(boolean security, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("security", security).apply();
    }

    static void setSports(boolean sports, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("sports", sports).apply();
    }

    static void setVacations(boolean vacations, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("vacations", vacations).apply();
    }

    static void setTrips(boolean trips, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("trips", trips).apply();
    }

    static void setFood(boolean food, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("food", food).apply();
    }

    static void setFamily(boolean family, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("family", family).apply();
    }

    static void setFashion(boolean fashion, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("fashion", fashion).apply();
    }

    static void setMusic(boolean music, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("music", music).apply();
    }

    static void setInnovation(boolean innovation, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("innovation", innovation).apply();
    }

    static void setEconomics(boolean economics, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("economics", economics).apply();
    }

    static void setReligion(boolean religion, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("religion", religion).apply();
    }

    static void setHistory(boolean history, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("history", history).apply();
    }

    static void setVehicles(boolean vehicles, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("vehicles", vehicles).apply();
    }

    static void setEducation(boolean education, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("education", education).apply();
    }

    static void setHealth(boolean health, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("health", health).apply();
    }

    static void setMedia(boolean media, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("media", media).apply();
    }

    static void setRememberMe(boolean rememberMe, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("remember_me", rememberMe).apply();
    }

    static void resetServerIP(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("server_ip", "127.0.0.1").apply();
    }

    static void resetServerPort(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putInt("server_port", 49151).apply();
    }

    static void resetUsername(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("username", "").apply();
    }

    static void resetFirstRun(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("is_first_run", true).apply();
    }
}
